<?php

namespace App\Controller;

use App\Entity\User;
use App\Helper\GlobalHelper;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Constraints\Json;

class UserController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private UserRepository $userRepository,
    ) {
    }

    #[Route('/users', name: 'PostCollection', methods: 'POST')]
    public function __invoke(mixed $data, Request $request, UserPasswordHasherInterface $userPasswordHasher): JsonResponse
    {
        $payload = json_decode($request->getContent(), true);

        if ($this->userRepository->findOneBy(['email' => $payload['email']])) {
            return new JsonResponse(
                [
                    'status' => false,
                    'message' => 'Cet email existe déjà, veuillez vous connecter.'
                ]
            );
        }

        $data = new User();

        $data->setEmail($payload['email'])->setPassword($userPasswordHasher->hashPassword($data, $payload['password']))->setRoles(['ROLE_USER']);

        $token = GlobalHelper::createToken($data->getEmail(), $data->getUserIdentifier());

        $data->setConfirmationToken($token);
        $this->em->persist($data);

        $this->em->flush();

        return new JsonResponse(
            [
                'status' => true,
                'message' => 'L\'utilisateur créé avec succès'
            ]
        );
    }
}
