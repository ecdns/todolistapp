<?php

namespace App\Helper;

final class GlobalHelper
{
    /**
     * @param  string $email
     * @param  string $username
     * @return string
     */
    public static function createToken(string $email, string $username): string
    {
        $token = base64_encode(
            hash_hmac('sha256', json_encode([$email, $username]), true)
        );

        $arrayToken = str_split($token);
        $arrayNoAutoriseChar = ['/','+','='];

        foreach ($arrayToken as $index => $character) {
            foreach ($arrayNoAutoriseChar as $noAutorizChar) {
                if ($character === $noAutorizChar) {
                    unset($arrayToken[$index]);
                }
            }
        }

        return implode(array_values($arrayToken));
    }

}
